# README

## About

Agatha -- Machine-learning powered literature management.

The concept for this software and a few parts of the code are based on the
shakespeare project (https://github.com/benjaminaschultz/shakespeare) by
Benjamin Schultz.

## Authors

  * Carl Simon Adorf, csadorf@umich.edu
  * Wenbo Shen, shenwb@umich.edu

## Documentation

The full documentation is hosted at [agatha.readthedocs](http://agatha.readthedocs.io/).

### Quickstart

    # Get new sources:
    agatha fetch -s sources.py > new.txt

    # Train interactively
    agatha score new.txt --ignore train.txt >> train.txt

    # Batch training
    bib2uri library.bib > library.txt
    agatha score library.txt -b 0.1 > train.txt

    # Train model
    agatha train train.txt > model.json

    # Use model to pre-score other literature
    agatha score new.txt --model model.json > scored.txt

### Configuration

``~/agatha_config.py``:

    from agatha.sources import JournalFeed as JF
    from agatha.sources import BibTexLibrary

    sources = [
       BibTexLibrary('~/library.bib'),
       BibTexLibrary('~/library/*.bib'),
       JF('http://feeds.nature.com/nature/rss/current?format=xml'),
       JF('https://www.sciencemag.org/rss/current.xml'),
    ]

### File formats

    #train.txt
    # {URI} [SCORE]
    URI1 0.6
    URI2 1.0
    URI3 0.1
    URI4 0.0
    ...

    #input.txt
    URI1
    URI2
    ...