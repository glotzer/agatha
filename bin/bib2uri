#!/usr/bin/env python
# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the BSD License.
import sys
import argparse
import logging

import bibtexparser


logger = logging.getLogger(__name__)


def main(args):
    with open(args.bibtexfile) as file:
        bibtex_db = bibtexparser.load(file)
        for entry in bibtex_db.entries:
            doi = entry.get('doi')
            if doi is None:
                logger.info("No DOI for '{}'.".format(entry))
            else:
                if doi.startswith('doi:'):
                    print('doi://' + doi[len('doi:'):])
                else:
                    print('doi://' + doi)
    return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Extract URIs from a BibTeX file.")
    parser.add_argument(
        'bibtexfile',
        type=str,
        help="The filename of a BibTex file.")
    args = parser.parse_args()
    logging.basicConfig(level=logging.WARNING)
    sys.exit(main(args))
