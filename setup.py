# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the Modified BSD License.
import sys
from setuptools import setup, find_packages

if sys.version_info < (3, 3, 0):
    print("Error: agatha requires python version >= 3.3")
    sys.exit(1)

setup(
    name='agatha',
    version='0.1.0',
    packages=find_packages(),
    scripts = ['bin/bib2uri'],
    zip_safe=True,

    author='Carl Simon Adorf, Wenbo Shen',
    author_email='csadorf@umich.edu',
    description="Machine-learning powered literature management.",

    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: Modified BSD License",
    ],

    install_requires=[
        'numpy', 'sklearn', 'requests', 'bibtexparser', 'feedparser',
        ],

    entry_points={
        'console_scripts': [
            'agatha = agatha.__main__:main',
        ],
    },
)
