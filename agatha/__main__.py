#!/usr/bin/env python
# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the Modified BSD License.
import os
import sys
import argparse
import logging
import textwrap
from importlib.machinery import SourceFileLoader

from .core import parse_doc, build_local_cache, CACHE
from . import training

logger = logging.getLogger(__name__)

FN_DEFAULT_CONFIG = os.path.expanduser('~/agatha_config.py')


def lookup_uri(uri, config=None, sources=None, required_fields=None):
    if required_fields is None:
        required_fields = ('title', 'author')
    if sources is None:
        sources = (CACHE, )
    for source in sources:
        doc = source.get(uri)
        if doc is None:
            continue
        for field in required_fields:
            if field not in doc:
                continue
        return doc
    if doc:
        return doc
    else:
        logger.warning("Failed to lookup '{}'.".format(uri))
        return {}


def _read_config(fn_config):
    try:
        return SourceFileLoader(fn_config, fn_config).load_module()
    except FileNotFoundError:
        return None


def _read_uri_file(file):
    for line in file:
        if line.startswith('#'):
            continue
        try:
            uri, score = line.strip().split()
        except ValueError:
            uri = line.strip()
            score = None
        if score is None:
            yield uri, None
        else:
            yield uri, float(score)


def _format_doc_(doc):
    yield "Journal: {}".format(doc.get('journal'))
    yield "Title: {}".format(doc.get('title'))
    yield "Authors: {}".format(doc.get('author'))
    yield "Keywords: {}".format(doc.get('keywords'))
    yield "Abstract: {}".format(doc.get('abstract'))


def _format_doc(doc):
    for line in _format_doc_(doc):
        yield from textwrap.wrap(line)


def _score_interactive(uri, score=None):
    print("\n\nScoring '{}':".format(uri), file=sys.stderr)
    doc = lookup_uri(uri)
    if not doc:
        print(
            "Failed to lookup document.", file=sys.stderr)
    for line in _format_doc(doc):
        print(line, file=sys.stderr)
    default = 's' if score is None else str(round(5 * score))
    for i in range(5):
        try:
            print("Enter score [s|1..5] ({}): ".format(default),
                  end='', file=sys.stderr)
            r = input()
            if r == '':
                r = default
            if r == 's':
                return
            else:
                score = int(r)
                assert score >= 0 and score <= 5
                break
        except ValueError:
            print("Invalid value! Must be integer in range [0, 5]!",
                  file=sys.stderr)
        except AssertionError:
            print("Outside of range [0, 5] !", file=sys.stderr)
    else:
        raise RuntimeError("Too many failed attempts!")
    if score and score != 's':
        return int(score) / 5


def main_fetch(args, config=None):
    sources = getattr(config, 'sources', [])
    build_local_cache(config)
    for src in sources:
        for doc in src.fetch():
            try:
                print(parse_doc(doc))
            except ValueError:
                logger.warning("Did not identify URI for '{}'.".format(doc))


def main_score(args, config=None):
    if args.score and args.model:
        raise ValueError(
            "You cannot specify -s/--score and -m/--model at the same time.")
    if args.score:
        batch_score = float(args.score)
        if not batch_score >= 0 and batch_score <= 1.0:
            raise ValueError(batch_score)
    else:
        batch_score = None
    if args.uris == '-':
        raise NotImplementedError()
    if args.ignore:
        with open(args.ignore) as file:
            ignore = set((l.split()[0] for l in file.readlines()))
    else:
        ignore = set()
    build_local_cache(config)
    if args.model:
        with open(args.model) as file:
            modeldata = training.deserialize_model(file.read())
            with open(args.uris) as urifile:
                uris = (l[0] for l in _read_uri_file(urifile))
                uris = [l for l in set(uris) if l not in ignore]
                resources = ((uri, lookup_uri(uri)) for uri in uris)
                resources = [r for r in resources if r[1] is not None]
                docs = [r[1] for r in resources]
                scores = training.score_with_model(docs, ** modeldata)
                results = [(r[0], s) for r, s in zip(resources, scores)]
                for u, s in reversed(sorted(results, key=lambda x: x[1])):
                    print(u, s)
        return
    with open(args.uris) as urifile:
        for uri, default_score in _read_uri_file(urifile):
            if uri in ignore:
                logger.info("Skipping '{}'".format(uri))
                continue
            if batch_score:
                score = batch_score
            else:
                score = _score_interactive(uri, default_score)
            if score is not None:
                print(uri, score)


def main_train(args, config=None):
    build_local_cache(config)
    with open(args.scoreduris) as file:
        resources = []
        scores = []
        for uri, score in _read_uri_file(file):
            doc = lookup_uri(uri)
            if doc is None:
                logger.warning("No resource for '{}'.".format(uri))
                continue
            if 'abstract' not in doc:
                logger.warning("No abstract for '{}'.".format(uri))
            resources.append(doc)
            scores.append(score)
        keywords, model = training.train(resources, scores)
        print(training.serialize_model(keywords, model))


def main_export(args, config=None):
    raise NotImplementedError()


def main():
    parser = argparse.ArgumentParser(
        description="Agatha - ML powered literature management.")
    parser.add_argument(
        '-c', '--config',
        type=str,
        default=FN_DEFAULT_CONFIG,
        help="The filename of the configuration file.")
    parser.add_argument(
        '-d', '--debug',
        action='store_true',
        help="Use debugging output.")

    subparsers = parser.add_subparsers()

    parser_fetch = subparsers.add_parser('fetch')
    parser_fetch.add_argument(
        '-s', '--sources',
        type=str,
        nargs='+',
        help="Filename to module that define sources.")
    parser_fetch.set_defaults(func=main_fetch)

    parser_score = subparsers.add_parser('score')
    parser_score.add_argument(
        'uris',
        type=str,
        default='-',
        help="The filename of a file containint uniform resource "
             "identifiers (URIs) of resources that shall be scored. "
             "Use '-' to read from the standard input.")
    parser_score.add_argument(
        '-s', '--score',
        type=float,
        help="Apply this score to all selected resources. "
             "Omit this argument will prompt for scores interactively.")
    parser_score.add_argument(
        '-i', '--ignore',
        type=str,
        help="The filename of a file containing URIs of "
             "resources that should be ignored.")
    parser_score.add_argument(
        '-m', '--model',
        type=str,
        help="The filename of the file containing the trained model.")
    parser_score.set_defaults(func=main_score)

    parser_train = subparsers.add_parser('train')
    parser_train.add_argument(
        'scoreduris',
        type=str,
        default='-',
        help="The filename of a file containint uniform resource "
             "identifiers (URIs) of resources and their score. "
             "Use '-' to read from the standard input.")
    parser_train.set_defaults(func=main_train)

    parser_export = subparsers.add_parser('export')
    parser_export.add_argument(
        'scoreduris',
        type=str,
        default='-',
        help="The filename of a file containint uniform resource "
             "identifiers (URIs) of resources and their score. "
             "Use '-' to read from the standard input.")
    parser_export.set_defaults(func=main_export)

    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.debug else logging.WARNING)

    if not hasattr(args, 'func'):
        parser.print_usage()
        sys.exit(2)

    try:
        config = _read_config(args.config)
        args.func(args, config)
    except Exception as error:
        print("Error: {}".format(error), file=sys.stderr)
        raise
        if args.debug:
            raise
        else:
            sys.exit(1)
    else:
        sys.exit(0)

if __name__ == '__main__':
    main()
