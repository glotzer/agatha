# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the Modified BSD License.
import numpy as np
from sklearn import linear_model
import operator
import json
from collections import defaultdict

TRIVIAL_WORDS = (
    'the', 'of', 'and', 'a', 'to', 'in', 'for', 'we', 'is', 'that',
    'with', 'at', 'this', 'are', 'on', 'these', 'as', 'into', 'which',
    'by', 'from', 'out', 'it', 'be', 'between', 'an', 'can', 'were',
    'also', 'been', 'or', 'have', 'has', 'not', 'was', 'but', 'through',
    'other', 'than', 'their')


def _extract_keywords(resource):
    yield resource.get('journal', '').lower().replace(' ', '')
    yield from resource.get('author', '').lower().split()
    yield from resource.get('title', '').lower().split()
    yield from resource.get('title', '').lower().split()
    yield from resource.get('abstract', '').lower().split()


def _count_keywords(resources, keywords):
    """Map the resources into the word (feature) space."""
    X = np.zeros((len(resources), len(keywords)))
    for i, resource in enumerate(resources):
        r_kw = ' '.join(list(_extract_keywords(resource)))
        for j, kw in enumerate(keywords):
            X[i][j] = r_kw.count(kw)
    return X


def _find_popular_keywords(resources):
    '''
    function to find the top N non-trivial keywords
    with the number of their occurences
    '''
    # put all abstract together
    word_bag = []
    for resource in resources:
        try:
            # simple test to make sure doi
            # and abstract are presresource
            # put all abstract together
            word_bag.extend(_extract_keywords(resource))
        except KeyError:
            pass

    # Filter trivial words
    word_bag = [
        w for w in word_bag if len(w) > 2 and w not in set(TRIVIAL_WORDS)]

    count = defaultdict(int)
    for word in word_bag:
        count[word] += 1

    keywords = reversed(sorted(count.items(), key=operator.itemgetter(1)))
    return [w[0] for w in keywords]


def _fit(X, y):
    """Fit the model using a ridge regression model."""
    clf = linear_model.Ridge(alpha=0.5)
    clf.fit(np.asarray(X), np.asarray(y))
    return clf


def train(resources, scores, num_dim=500):
    """Train the model on resources and scores."""
    assert len(resources) == len(scores)
    keywords = _find_popular_keywords(resources)[:num_dim]
    X = _count_keywords(resources, keywords)
    assert len(X)
    model = _fit(X, scores)
    return keywords, model


def score_with_model(resources, keywords, model):
    X = _count_keywords(resources, keywords)
    if len(X):
        scores = model.predict(np.asarray(X))
        scores[scores < 0.0] = 0
        scores[scores > 1.0] = 1.0
        return scores


def serialize_model(keywords, model):
    blob = json.dumps({
        'keywords': keywords,
        'model': {
            'coef': model.coef_.tolist(),
            'intercept': model.intercept_},
        'version': 1}, indent=2)
    deserialize_model(blob)
    return blob


def deserialize_model(blob):
    modeldata = json.loads(blob)
    version = modeldata.get('version', 1)
    if version == 1:
        model = modeldata.get('model', {})
        if 'coef' in model and 'intercept' in model:
            clf = linear_model.Ridge(alpha=0.5)
            clf.coef_ = np.asarray(modeldata['model']['coef'])
            clf.intercept_ = modeldata['model']['intercept']
            return dict(keywords=modeldata['keywords'], model=clf)
        else:
            raise ValueError("Unable to identify model!")
    else:
        raise ValueError("Unable to deserialize modeldata.")
