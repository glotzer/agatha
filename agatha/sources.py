# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the Modified BSD License.
import logging
import glob
import re

import requests
import bibtexparser
import feedparser

logger = logging.getLogger(__name__)

PATTERN_DOI = '(10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+)'


def _parse_doi(doc):
    s = str(doc).replace('2%2F', '/')
    m = re.findall(PATTERN_DOI, s)
    if m:
        return m[0]


class BibTexLibrary:

    def __init__(self, filename):
        self.filename = filename

    def fetch(self):
        for fn in glob.glob(self.filename):
            with open(fn) as file:
                bibtex_db = bibtexparser.load(file)
                for entry in bibtex_db.entries:
                    yield entry

    def get(self, uri, default=None):
        for entry in self.fetch():
            if uri.startswith('doi://'):
                doi = entry.get('doi')
                if not doi.startswith('doi://'):
                    doi = 'doi://' + doi
                if uri == doi:
                    return entry
        return default


class JournalRSSFeed:
    """Read a scientific journal RSS feed.

    Code adapted from the shakespeare project:

        https://github.com/benjaminaschultz/shakespeare/
    """

    def __init__(self, address):
        self.address = address

    def fetch(self):
        entries = feedparser.parse(self.address)['entries']
        for entry in entries:
            keys = ('title', 'author', 'abstract', 'url', 'doi')
            doc = {k: entry[k] for k in keys if k in entry}
            doc.setdefault('abstract', entry.get('summary'))
            doc.setdefault('url', entry.get('link'))
            if 'doi' not in doc:
                doc['doi'] = _parse_doi(doc)
            doc = {k: v for k, v in doc.items() if v is not None}
            if doc:
                yield doc

    def get(self, uri, default=None):
        return default


class Dxdoiorg:

    def fetch(self):
        return
        yield

    def get(self, uri, default=None):
        if not uri.startswith('doi://'):
            raise ValueError(uri)
        query = "http://dx.doi.org/{doi}".format(doi=uri[len('doi://'):])
        headers = {'accept': 'text/x-bibliography; style=bibtex'}
        logger.info("Quering dx.doi.org for: '{}'.".format(query))
        r = requests.get(query, headers=headers)
        if r.status_code == 200:
            bib = bibtexparser.loads(r.text.replace(', ', ',\n'))
            return bib.entries[0]
        else:
            logger.debug(
                "Failed to lookup '{}', status: {}.".format(
                    uri, r.status_code))
            return default
