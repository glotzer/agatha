# Copyright (c) 2016 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the Modified BSD License.
import os
import logging
import json

logger = logging.getLogger(__name__)

CACHE = dict()


class ResourceCache:

    def __init__(self, filename):
        self.filename = filename
        self.cache = None

    def _with_open_cache(self):
        if self.cache is None:
            raise RuntimeError("Cache not opened!")

    def __enter__(self):
        try:
            with open(self.filename) as file:
                self.cache = json.load(file)
        except FileNotFoundError:
            self.cache = dict()

    def __exit__(self, e_val, e_type, tb):
        with open(self.filename, 'w') as file:
            json.dump(self.cache, file)
        return False

    def __getitem__(self, key):
        self._with_open_cache()
        return self.cache.__getitem__(key)

    def __setitem__(self, key, value):
        self._with_open_cache()
        return self.cache.__setitem__(key, value)

    def __delitem__(self, key, value):
        self._with_open_cache()
        self.cache__delitem__(key)

    def __len__(self):
        self._with_open_cache()
        return len(self.cache)


def get_cache(config):
    fn_cache = getattr(config, 'FN_CACHE', os.path.expanduser(
        '~/.cache/agatha_cache.json'))
    os.makedirs(os.path.dirname(fn_cache), exist_ok=True)
    return ResourceCache(fn_cache)


def parse_doc(doc):
    if 'doi' in doc:
        return 'doi://' + doc['doi']
    elif 'arxiv' in doc:
        return 'arxiv://' + doc['arxiv']
    elif 'url' in doc:
        return doc['url']
    else:
        raise ValueError(doc)


def build_local_cache(config, cache=None):
    if cache is None:
        cache = get_cache(config)
    with cache:
        sources = getattr(config, 'sources', [])
        for src in sources:
            for doc in src.fetch():
                try:
                    uri = parse_doc(doc)
                    cache[uri] = doc
                except ValueError:
                    logger.debug("Unable to parse document '{}'.".format(doc))
        CACHE.update(cache.cache)
